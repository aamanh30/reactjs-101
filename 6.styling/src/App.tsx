import React from 'react';
import './App.css';
import Persons from './components/persons';
import CssModule from './components/styling/cssModule';
import ExternalCSS from './components/styling/externalCSS';
import InlineCSS from './components/styling/inlineCSS';

class App extends React.Component {
  constructor(props: any) {
    super(props);

    this.state = {
      name: 'Aaman'
    };
  }

  render() {
    const props = {
      name: (this.state as any).name
    };
    return (
      <div className="App">
        <main>
          <Persons {...props} />
          <ExternalCSS />
          <InlineCSS />
          <CssModule />
        </main>
      </div>
    );
  }
}

export default App;
