import React, { Component } from 'react'

export class InlineCSS extends Component {
    render() {
        const style = {
            backgroundColor: '#f8f8f8',
            color: '#0000ff',
            fontSize: '72px'
        };
        return (
            <div>
                <h2 style={style}>INLINE CSS</h2>
            </div>
        )
    }
}

export default InlineCSS;