import React, { Component } from 'react'
import classes from './styles.module.css';

export class CssModule extends Component {
    render() {
        return (
            <div>
                <h3 className={classes.success}>SUCCESS</h3>
                <h3 className={classes.info}>INFO</h3>
            </div>
        )
    }
}

export default CssModule;