import React, { Component } from 'react';

export class WelcomeMessage extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      loggedIn: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    this.setState(
      ({ loggedIn }: any) => ({
        loggedIn: !loggedIn
      }),
      () => console.log(this.state)
    );
  };

  getMessage() {
    const { loggedIn } = this.state;
  }

  render() {
    const { loggedIn } = this.state;
    const buttonTag = (
      <button onClick={this.handleClick}>
        {loggedIn ? 'LOGOUT' : 'LOGIN'}
      </button>
    );
    if (loggedIn) {
      const { name } = this.props;
      return (
        <>
          <div>Welcome {name}</div>
          {buttonTag}
        </>
      );
    } else {
      return (
        <>
          <div>Welcome GUEST</div>
          {buttonTag}
        </>
      );
    }
  }
}

export default WelcomeMessage;
