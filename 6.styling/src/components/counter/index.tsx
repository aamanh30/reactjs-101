import React, { Component } from 'react';

class Counter extends Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            counter: 0
        };
    }

    handleClick = () => {
        this.setState((previousState: any) => ({
            counter: previousState.counter + 1
        }), () => {
            console.log(`STATE: `, this.state);
        });
    }
    render() {
        return (
            <div>
                <h2>Counter: { this.state.counter }</h2>
                <button onClick={this.handleClick}>+</button>
            </div>
        )
    }
}

export default Counter
