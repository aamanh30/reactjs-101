import React from 'react';
import Subscription from './components/subscription';
import './App.css';
import Counter from './components/counter';
import EventBinding from './components/event-binding';

class App extends React.Component {
  constructor(props: any) {
    super(props);

    this.state = {
      name: 'Aaman'
    };
  }

  render() {
    const { name } = this.state as any;
    return (
      <div className="App">
        <main>
          <EventBinding />
        </main>
      </div>
    );
  }
}

export default App;
