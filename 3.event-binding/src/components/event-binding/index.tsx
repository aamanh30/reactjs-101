import React, { Component } from 'react'

export class EventBinding extends Component<any, any> {
    constructor(props: any) {
        super(props)
    
        this.state = {
            message: ''
        };

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(event: any) {
        this.setState((prevState: any) => ({
            message: prevState.message + new Date().getTime()
        }));
    }
    
    render() {
        const { message } = this.state;
        return (
            <div>
                <button onClick={this.handleClick}>CLICK</button>    
                <p>{message}</p>  
            </div>
        )
    }
}

export default EventBinding
