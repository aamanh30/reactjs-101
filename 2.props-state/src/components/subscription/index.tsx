import React, { Component, Fragment } from 'react';

export interface SubscriptionProps {
    name: string;
}

export interface SubscriptionState {
    subscribed: boolean;
}

class Subscription extends Component<SubscriptionProps, SubscriptionState> {
  constructor(props: any) {
    super(props);
    this.init();
  }

  init() {
    this.state = {
        subscribed: false
    };
  }

  handleClick = (event: any): void => {
    const state = {
        ...this.state,
        subscribed: !this.state.subscribed
    }
    this.setState({ ...state });
  };

  render = () => {
    const { name = '' } = this.props;
    const { subscribed = false } = this.state;
    return (
      <Fragment>
        <h1>{subscribed ? `Welcome back, ${name}` : `Hello ${name}`}</h1>
        <button onClick={this.handleClick}>
          {subscribed ? 'UNSUBSCRIBE' : 'SUBSCRIBE'}
        </button>
      </Fragment>
    );
  };
}

export default Subscription;
