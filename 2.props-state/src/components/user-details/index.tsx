import React from 'react';
import { userProperties } from '../../configs';

export const UserDetails = ({ id, name, salary, onChange }: any) => {
  return (
    <tr>
      <td>
        <p>{id}</p>
      </td>
      <td>
        <input
          type="text"
          value={name}
          onChange={event => onChange(id, userProperties.name, event)}
        />
      </td>
      <td>
        <input
          type="text"
          value={salary}
          onChange={event => onChange(name, userProperties.salary, event)}
        />
      </td>
    </tr>
  );
};
