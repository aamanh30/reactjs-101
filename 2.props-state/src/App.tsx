import React from 'react';
import Subscription from './components/subscription';
import './App.css';
import Counter from './components/counter';

class App extends React.Component {
  render() {
    const props = {
      name: 'Aaman'
    };
    return <div className="App">
    <main>
      <Subscription { ...props } />

      <Counter />
    </main>
  </div>
  }
}

export default App;