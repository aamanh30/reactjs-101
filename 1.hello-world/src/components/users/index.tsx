import React, { useState, useEffect } from 'react';
import { UserDetails } from '../user-details';
import {
  messages,
  messageTypes,
  messageTypesClasses,
  usersConfig
} from '../../configs';

export const Users = () => {
  const [users, setUsers] = useState(usersConfig);
  const [message, setMessage] = useState(messages.default);

  useEffect(() => {
    if (message) {
      setTimeout(() => setMessage(messages.default), 3000);
    }
  }, [message]);
  const getClassName = ({ type = '' }: any): string => {
    switch (type) {
      case messageTypes.info.toString():
        return messageTypesClasses.info;
      case messageTypes.success.toString():
        return messageTypesClasses.success;
      case messageTypes.danger.toString():
        return messageTypesClasses.danger;
      default:
        return '';
    }
  };
  const onChange = (id: string | number, prop: string, event: any): void => {
    if (!event || !event.target || !event.target.value) {
      const actionMessage = messages.userUpdateFailed;
      return setMessage(actionMessage);
    }
    const index = users.findIndex(userItem => id === userItem.id);
    if (index < 0) {
      const actionMessage = messages.usersNotFound;
      return setMessage(actionMessage);
    }

    users[index] = {
      ...users[index],
      [prop]: event.target.value
    };

    setUsers([...users]);
    const actionMessage = messages.userUpdateSuccess;
    return setMessage(actionMessage);
  };
  return (
    <>
      <section>
        <table>
          <thead>
            <tr>
              <td>ID</td>
              <td>NAME</td>
              <td>SALARY</td>
            </tr>
          </thead>
          <tbody>
            {users &&
              users.length &&
              users.map(({ name, id, salary }) => (
                <UserDetails
                  key={id}
                  id={id}
                  name={name}
                  salary={salary}
                  onChange={onChange}
                />
              ))}
            {!users ||
              (!users.length && (
                <tr>
                  <td colSpan={3}>{messages.usersNotFound}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </section>
      {message && message.content && (
        <section>
          <p className={getClassName(message)}>{message.content}</p>
        </section>
      )}
    </>
  );
};
