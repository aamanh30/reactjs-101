import React from 'react';
import { Users } from './components/users';
import './App.css';

function App() {
  return (
    <div className="App">
      <main>
        <Users />
      </main>
    </div>
  );
}

export default App;
