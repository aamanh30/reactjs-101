import React from 'react';
import './App.css';
import Persons from './components/persons';

class App extends React.Component {
  constructor(props: any) {
    super(props);

    this.state = {
      name: 'Aaman'
    };
  }

  render() {
    const props = {
      name: (this.state as any).name
    };
    return (
      <div className="App">
        <main>
          <Persons {...props} />
        </main>
      </div>
    );
  }
}

export default App;
