export enum userProperties {
    id = 'id',
    name = 'name',
    salary = 'salary'
};

export const usersConfig = [
    {
        id: 1001,
        name: 'Aaman',
        salary: 20000
    },
    {
        id: 1002,
        name: 'Aman',
        salary: 25000
    },
    {
        id: 1003,
        name: 'Amn',
        salary: 40000
    },
    {
        id: 1004,
        name: 'Aamaan',
        salary: 60000
    },
    {
        id: 1005,
        name: 'Amaan',
        salary: 50000
    }
];
export enum messageTypes {
    success,
    info,
    danger
};
export enum messageTypesClasses {
    success = 'text-success',
    info = 'text-info',
    danger = 'text-danger'
};

export const messages = {
    default: {
        type: '',
        content: ''    
    },
    usersNotFound: {
        type: messageTypes.info.toString(),
        content: `No users found`
    },
    userUpdateSuccess: {
        type: messageTypes.success.toString(),
        content: `User details have been updated successfully`
    },
    userUpdateFailed: {
        type: messageTypes.danger.toString(),
        content: `User details could not be updated`
    }
};