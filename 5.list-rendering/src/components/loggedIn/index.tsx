import React, { Component } from 'react';

export class LoggedIn extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      loggedIn: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    this.setState(
      ({ loggedIn }: any) => ({
        loggedIn: !loggedIn
      }),
      () => console.log(this.state)
    );
  };

  render() {
    const { loggedIn } = this.state;
    const { name } = this.props;
    let template;
    if(loggedIn ) {
        template = <p>Welcome {name}</p>;
    } else {
        template = <p>Welcome GUEST</p>;
    }
    return (
      <div>
        <section>
          <h2>ELEMENT</h2>
          {template}
        </section>
        <section>
          <h2>TERNARY OPERATOR</h2>
          {loggedIn ? (<p>Welcome {name}</p>) : (<p>Welcome GUEST</p>)}
        </section>
        <section>
          <h2>SHORT CIRCUIT OPERATOR</h2>
          {loggedIn && <p>Welcome {name}</p>}
          {!loggedIn && <p>Welcome GUEST</p>}
        </section>
        <section>
          <button onClick={this.handleClick}>{loggedIn ? `LOG OUT`: `LOGIN`}</button>
        </section>
      </div>
    );
  }
}

export default LoggedIn;
