import React, { Component } from 'react';
import { userProperties } from '../../configs';

class PersonDetails extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {};
  }

  render() {
    const { id, name, salary, onChange } = this.props;
    return (
      <section>
        <div>
          <p>{id}</p>
        </div>
        <div>
          <input
            type="text"
            value={name}
            onChange={event => onChange(id, userProperties.name, event)}
          />
        </div>
        <div>
          <input
            type="text"
            value={salary}
            onChange={event => onChange(id, userProperties.salary, event)}
          />
        </div>
      </section>
    );
  }
}

export default PersonDetails;
