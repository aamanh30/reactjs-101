import React, { Component } from 'react';
import { messages, usersConfig } from '../../configs';
import Person from '../person';

export class Persons extends Component {
  constructor(props: any) {
    super(props);

    this.state = {
      persons: usersConfig
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(id: string | number, prop: string, event: any): void {
      console.log(id, prop, event.target.value);
    if (!event || !event.target || !event.target.value) {
      const actionMessage = messages.userUpdateFailed;
      return;
    }
    const { persons } = this.state as any;
    const index = persons.findIndex((userItem: any) => id === userItem.id);
    console.log(index);
    if (index < 0) {
      const actionMessage = messages.usersNotFound;
      return;
    }

    persons[index] = {
      ...persons[index],
      [prop]: event.target.value
    };

    this.setState({
      persons: [...persons]
    });
    const actionMessage = messages.userUpdateSuccess;
  }
  render() {
    const { persons } = this.state as any;
    const personsList = persons.map(({ id, name, salary }: any) => (
      <Person
        key={id}
        id={id}
        name={name}
        salary={salary}
        onChange={this.onChange}
      />
    ));
    return <div className="persons">{personsList}</div>;
  }
}

export default Persons;
