import React from 'react';
import './App.css';
import LoggedIn from './components/loggedIn';

class App extends React.Component {
  constructor(props: any) {
    super(props);

    this.state = {
      name: 'Aaman'
    };
  }

  render() {
    const props = {
      name: (this.state as any).name
    };
    return (
      <div className="App">
        <main>
          <LoggedIn {...props} />
        </main>
      </div>
    );
  }
}

export default App;
